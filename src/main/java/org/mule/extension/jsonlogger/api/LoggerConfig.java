package org.mule.extension.jsonlogger.api;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import org.apache.commons.lang.builder.ToStringBuilder;
import java.util.HashMap;
import com.fasterxml.jackson.annotation.JsonIgnore;
import java.util.Map;
import org.mule.runtime.extension.api.annotation.param.ParameterGroup;
import org.mule.runtime.extension.api.annotation.param.display.Placement;
import org.mule.runtime.api.meta.ExpressionSupport;
import org.mule.runtime.extension.api.annotation.Expression;
import org.mule.runtime.extension.api.annotation.param.Parameter;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "globalSettings", "jsonOutput" })
public class LoggerConfig
{
    @JsonProperty("globalSettings")
    @Parameter
    @Expression(ExpressionSupport.NOT_SUPPORTED)
    @Placement(order = 1)
    @ParameterGroup(name = "Global Settings")
    private GlobalSettings globalSettings;
    @JsonProperty("jsonOutput")
    @Parameter
    @Expression(ExpressionSupport.NOT_SUPPORTED)
    @ParameterGroup(name = "JSON Output")
    private JsonOutput jsonOutput;
    @JsonIgnore
    private Map<String, Object> additionalProperties;
    
    public LoggerConfig() {
        this.additionalProperties = new HashMap<String, Object>();
    }
    
    @JsonProperty("globalSettings")
    public GlobalSettings getGlobalSettings() {
        return this.globalSettings;
    }
    
    @JsonProperty("globalSettings")
    public void setGlobalSettings(final GlobalSettings globalSettings) {
        this.globalSettings = globalSettings;
    }
    
    @JsonProperty("jsonOutput")
    public JsonOutput getJsonOutput() {
        return this.jsonOutput;
    }
    
    @JsonProperty("jsonOutput")
    public void setJsonOutput(final JsonOutput jsonOutput) {
        this.jsonOutput = jsonOutput;
    }
    
    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString((Object)this);
    }
    
    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }
    
    @JsonAnySetter
    public void setAdditionalProperty(final String name, final Object value) {
        this.additionalProperties.put(name, value);
    }
    
    @Override
    public int hashCode() {
        return new HashCodeBuilder().append((Object)this.globalSettings).append((Object)this.jsonOutput).append((Object)this.additionalProperties).toHashCode();
    }
    
    @Override
    public boolean equals(final Object other) {
        if (other == this) {
            return true;
        }
        if (!(other instanceof LoggerConfig)) {
            return false;
        }
        final LoggerConfig rhs = (LoggerConfig)other;
        return new EqualsBuilder().append((Object)this.globalSettings, (Object)rhs.globalSettings).append((Object)this.jsonOutput, (Object)rhs.jsonOutput).append((Object)this.additionalProperties, (Object)rhs.additionalProperties).isEquals();
    }
}
