package org.mule.extension.jsonlogger.api;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import org.apache.commons.lang.builder.ToStringBuilder;
import java.util.HashMap;
import com.fasterxml.jackson.annotation.JsonIgnore;
import java.util.Map;
import org.mule.runtime.extension.api.annotation.param.display.Example;
import org.mule.runtime.api.meta.ExpressionSupport;
import org.mule.runtime.extension.api.annotation.Expression;
import org.mule.runtime.extension.api.annotation.param.display.Summary;
import org.mule.runtime.extension.api.annotation.param.Optional;
import org.mule.runtime.extension.api.annotation.param.Parameter;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "prettyPrint", "logLocationInfo", "disabledFields" })
public class JsonOutput
{
    @JsonProperty("prettyPrint")
    @Parameter
    @Optional(defaultValue = "true")
    @Summary("Indicate if log entries should be formatted or single line")
    @Expression(ExpressionSupport.SUPPORTED)
    private boolean prettyPrint;
    @JsonProperty("logLocationInfo")
    @Parameter
    @Optional(defaultValue = "true")
    @Summary("Indicate if location information should be logged")
    @Expression(ExpressionSupport.SUPPORTED)
    private boolean logLocationInfo;
    @JsonProperty("disabledFields")
    @Parameter
    @Optional
    @Summary("Indicate which fields should be disabled from logging separated by comma. They should match the exact name given in loggerProcessor.json schema")
    @Example("e.g. message, content")
    @Expression(ExpressionSupport.REQUIRED)
    private String disabledFields;
    @JsonIgnore
    private Map<String, Object> additionalProperties;
    
    public JsonOutput() {
        this.additionalProperties = new HashMap<String, Object>();
    }
    
    @JsonProperty("prettyPrint")
    public boolean isPrettyPrint() {
        return this.prettyPrint;
    }
    
    @JsonProperty("prettyPrint")
    public void setPrettyPrint(final boolean prettyPrint) {
        this.prettyPrint = prettyPrint;
    }
    
    @JsonProperty("logLocationInfo")
    public boolean isLogLocationInfo() {
        return this.logLocationInfo;
    }
    
    @JsonProperty("logLocationInfo")
    public void setLogLocationInfo(final boolean logLocationInfo) {
        this.logLocationInfo = logLocationInfo;
    }
    
    @JsonProperty("disabledFields")
    public String getDisabledFields() {
        return this.disabledFields;
    }
    
    @JsonProperty("disabledFields")
    public void setDisabledFields(final String disabledFields) {
        this.disabledFields = disabledFields;
    }
    
    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString((Object)this);
    }
    
    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }
    
    @JsonAnySetter
    public void setAdditionalProperty(final String name, final Object value) {
        this.additionalProperties.put(name, value);
    }
    
    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(this.prettyPrint).append(this.logLocationInfo).append((Object)this.disabledFields).append((Object)this.additionalProperties).toHashCode();
    }
    
    @Override
    public boolean equals(final Object other) {
        if (other == this) {
            return true;
        }
        if (!(other instanceof JsonOutput)) {
            return false;
        }
        final JsonOutput rhs = (JsonOutput)other;
        return new EqualsBuilder().append(this.prettyPrint, rhs.prettyPrint).append(this.logLocationInfo, rhs.logLocationInfo).append((Object)this.disabledFields, (Object)rhs.disabledFields).append((Object)this.additionalProperties, (Object)rhs.additionalProperties).isEquals();
    }
}
