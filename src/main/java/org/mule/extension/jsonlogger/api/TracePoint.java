package org.mule.extension.jsonlogger.api;

import java.util.HashMap;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import java.util.Map;

public enum TracePoint
{
    START("START"), 
    BEFORE_TRANSFORM("BEFORE_TRANSFORM"), 
    AFTER_TRANSFORM("AFTER_TRANSFORM"), 
    BEFORE_REQUEST("BEFORE_REQUEST"), 
    AFTER_REQUEST("AFTER_REQUEST"), 
    FLOW("FLOW"), 
    END("END"), 
    EXCEPTION("EXCEPTION");
    
    private final String value;
    private static final Map<String, TracePoint> CONSTANTS;
    
    private TracePoint(final String value) {
        this.value = value;
    }
    
    @Override
    public String toString() {
        return this.value;
    }
    
    @JsonValue
    public String value() {
        return this.value;
    }
    
    @JsonCreator
    public static TracePoint fromValue(final String value) {
        final TracePoint constant = TracePoint.CONSTANTS.get(value);
        if (constant == null) {
            throw new IllegalArgumentException(value);
        }
        return constant;
    }
    
    static {
        CONSTANTS = new HashMap<String, TracePoint>();
        for (final TracePoint c : values()) {
            TracePoint.CONSTANTS.put(c.value, c);
        }
    }
}
