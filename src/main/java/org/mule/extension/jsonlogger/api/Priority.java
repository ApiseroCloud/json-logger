package org.mule.extension.jsonlogger.api;

import java.util.HashMap;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import java.util.Map;

public enum Priority
{
    DEBUG("DEBUG"), 
    TRACE("TRACE"), 
    INFO("INFO"), 
    WARN("WARN"), 
    ERROR("ERROR");
    
    private final String value;
    private static final Map<String, Priority> CONSTANTS;
    
    private Priority(final String value) {
        this.value = value;
    }
    
    @Override
    public String toString() {
        return this.value;
    }
    
    @JsonValue
    public String value() {
        return this.value;
    }
    
    @JsonCreator
    public static Priority fromValue(final String value) {
        final Priority constant = Priority.CONSTANTS.get(value);
        if (constant == null) {
            throw new IllegalArgumentException(value);
        }
        return constant;
    }
    
    static {
        CONSTANTS = new HashMap<String, Priority>();
        for (final Priority c : values()) {
            Priority.CONSTANTS.put(c.value, c);
        }
    }
}
