package org.mule.extension.jsonlogger.api;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import org.apache.commons.lang.builder.ToStringBuilder;
import java.util.HashMap;
import com.fasterxml.jackson.annotation.JsonIgnore;
import java.util.Map;
import org.mule.runtime.extension.api.annotation.param.Optional;
import org.mule.runtime.extension.api.annotation.param.Parameter;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "applicationName", "applicationVersion", "environment" })
public class GlobalSettings
{
    @JsonProperty("applicationName")
    @Parameter
    @Optional(defaultValue = "${json.logger.application.name}")
    private String applicationName;
    @JsonProperty("applicationVersion")
    @Parameter
    @Optional(defaultValue = "${json.logger.application.version}")
    private String applicationVersion;
    @JsonProperty("environment")
    @Parameter
    @Optional(defaultValue = "${mule.env}")
    private String environment;
    @JsonIgnore
    private Map<String, Object> additionalProperties;
    
    public GlobalSettings() {
        this.additionalProperties = new HashMap<String, Object>();
    }
    
    @JsonProperty("applicationName")
    public String getApplicationName() {
        return this.applicationName;
    }
    
    @JsonProperty("applicationName")
    public void setApplicationName(final String applicationName) {
        this.applicationName = applicationName;
    }
    
    @JsonProperty("applicationVersion")
    public String getApplicationVersion() {
        return this.applicationVersion;
    }
    
    @JsonProperty("applicationVersion")
    public void setApplicationVersion(final String applicationVersion) {
        this.applicationVersion = applicationVersion;
    }
    
    @JsonProperty("environment")
    public String getEnvironment() {
        return this.environment;
    }
    
    @JsonProperty("environment")
    public void setEnvironment(final String environment) {
        this.environment = environment;
    }
    
    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString((Object)this);
    }
    
    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }
    
    @JsonAnySetter
    public void setAdditionalProperty(final String name, final Object value) {
        this.additionalProperties.put(name, value);
    }
    
    @Override
    public int hashCode() {
        return new HashCodeBuilder().append((Object)this.applicationName).append((Object)this.applicationVersion).append((Object)this.environment).append((Object)this.additionalProperties).toHashCode();
    }
    
    @Override
    public boolean equals(final Object other) {
        if (other == this) {
            return true;
        }
        if (!(other instanceof GlobalSettings)) {
            return false;
        }
        final GlobalSettings rhs = (GlobalSettings)other;
        return new EqualsBuilder().append((Object)this.applicationName, (Object)rhs.applicationName).append((Object)this.applicationVersion, (Object)rhs.applicationVersion).append((Object)this.environment, (Object)rhs.environment).append((Object)this.additionalProperties, (Object)rhs.additionalProperties).isEquals();
    }
}
