package org.mule.extension.jsonlogger.api;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import org.apache.commons.lang.builder.ToStringBuilder;
import java.util.HashMap;
import com.fasterxml.jackson.annotation.JsonIgnore;
import java.util.Map;
import org.mule.runtime.extension.api.annotation.param.display.Placement;
import org.mule.runtime.extension.api.annotation.param.Content;
import org.mule.runtime.api.metadata.TypedValue;
import org.mule.runtime.extension.api.runtime.parameter.ParameterResolver;
import org.mule.runtime.extension.api.annotation.param.display.Example;
import org.mule.runtime.extension.api.annotation.param.display.Summary;
import org.mule.runtime.extension.api.annotation.param.Optional;
import org.mule.runtime.extension.api.annotation.param.Parameter;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "priority", "tracePoint", "message", "content", "correlationId" })
public class LoggerProcessor
{
    @JsonProperty("priority")
    @Parameter
    @Optional(defaultValue = "INFO")
    @Summary("Logger priority")
    private Priority priority;
    @JsonProperty("tracePoint")
    @Parameter
    @Optional(defaultValue = "START")
    @Summary("Current processing stage")
    private TracePoint tracePoint;
    @JsonProperty("message")
    @Parameter
    @Summary("Message to be logged")
    @Example("Add a log entry")
    private String message;
    @JsonProperty("content")
    @Parameter
    @Optional(defaultValue = "#[output application/json ---\n{\n \tpayload: payload,\n \tattributes: attributes\n}]")
    @Summary("Remove if no content should be logged")
    @Content
    private ParameterResolver<TypedValue<Object>> content;
    @JsonProperty("correlationId")
    @Parameter
    @Optional(defaultValue = "#[correlationId]")
    @Placement(tab = "Advanced")
    private String correlationId;
    @JsonIgnore
    private Map<String, Object> additionalProperties;
    
    public LoggerProcessor() {
        this.additionalProperties = new HashMap<String, Object>();
    }
    
    @JsonProperty("priority")
    public Priority getPriority() {
        return this.priority;
    }
    
    @JsonProperty("priority")
    public void setPriority(final Priority priority) {
        this.priority = priority;
    }
    
    @JsonProperty("tracePoint")
    public TracePoint getTracePoint() {
        return this.tracePoint;
    }
    
    @JsonProperty("tracePoint")
    public void setTracePoint(final TracePoint tracePoint) {
        this.tracePoint = tracePoint;
    }
    
    @JsonProperty("message")
    public String getMessage() {
        return this.message;
    }
    
    @JsonProperty("message")
    public void setMessage(final String message) {
        this.message = message;
    }
    
    @JsonProperty("content")
    public ParameterResolver<TypedValue<Object>> getContent() {
        return this.content;
    }
    
    @JsonProperty("content")
    public void setContent(final ParameterResolver<TypedValue<Object>> content) {
        this.content = content;
    }
    
    @JsonProperty("correlationId")
    public String getCorrelationId() {
        return this.correlationId;
    }
    
    @JsonProperty("correlationId")
    public void setCorrelationId(final String correlationId) {
        this.correlationId = correlationId;
    }
    
    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString((Object)this);
    }
    
    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }
    
    @JsonAnySetter
    public void setAdditionalProperty(final String name, final Object value) {
        this.additionalProperties.put(name, value);
    }
    
    @Override
    public int hashCode() {
        return new HashCodeBuilder().append((Object)this.priority).append((Object)this.tracePoint).append((Object)this.message).append((Object)this.content).append((Object)this.correlationId).append((Object)this.additionalProperties).toHashCode();
    }
    
    @Override
    public boolean equals(final Object other) {
        if (other == this) {
            return true;
        }
        if (!(other instanceof LoggerProcessor)) {
            return false;
        }
        final LoggerProcessor rhs = (LoggerProcessor)other;
        return new EqualsBuilder().append((Object)this.priority, (Object)rhs.priority).append((Object)this.tracePoint, (Object)rhs.tracePoint).append((Object)this.message, (Object)rhs.message).append((Object)this.content, (Object)rhs.content).append((Object)this.correlationId, (Object)rhs.correlationId).append((Object)this.additionalProperties, (Object)rhs.additionalProperties).isEquals();
    }
}
