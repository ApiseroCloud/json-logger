package org.mule.extension.jsonlogger.internal;

import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.stream.Stream;
import javax.inject.Inject;
import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.beanutils.PropertyUtils;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.mule.extension.jsonlogger.api.LoggerProcessor;
import org.mule.runtime.api.component.location.ComponentLocation;
import org.mule.runtime.api.lifecycle.Initialisable;
import org.mule.runtime.api.lifecycle.InitialisationException;
import org.mule.runtime.api.meta.ExpressionSupport;
import org.mule.runtime.api.metadata.DataType;
import org.mule.runtime.api.metadata.TypedValue;
import org.mule.runtime.api.transformation.TransformationService;
import org.mule.runtime.extension.api.annotation.Expression;
import org.mule.runtime.extension.api.annotation.param.Config;
import org.mule.runtime.extension.api.annotation.param.ParameterGroup;
import org.mule.runtime.extension.api.runtime.operation.Result;
import org.mule.runtime.extension.api.runtime.parameter.CorrelationInfo;
import org.mule.runtime.extension.api.runtime.parameter.ParameterResolver;
import org.mule.runtime.extension.api.runtime.process.CompletionCallback;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class JsonloggerOperations implements Initialisable {
    private static final Logger jsonLogger = LoggerFactory.getLogger("org.mule.extension.jsonlogger.JsonLogger");
    private static final Logger log = LoggerFactory.getLogger("org.mule.extension.jsonlogger.JsonLoggerExtension");
    private static final Result VOID_RESULT = Result.builder().build();
    private static final ObjectMapper om;
    @Inject
    private TransformationService transformationService;

    public void logger(@ParameterGroup(name = "Logger") @Expression(ExpressionSupport.NOT_SUPPORTED) LoggerProcessor loggerProcessor, CorrelationInfo correlationInfo, ComponentLocation location, @Config JsonloggerConfiguration config, CompletionCallback<Void, Void> callback) {
        Long initialTimestamp = 0L;

        try {
            initialTimestamp = config.getCachedTimerTimestamp(correlationInfo.getEventId());
        } catch (Exception var23) {
            log.error("initialTimestamp could not be retrieved from the cache config. Defaulting to current System.currentTimeMillis()", var23);
            initialTimestamp = System.currentTimeMillis();
        }

        Long loggerTimestamp = System.currentTimeMillis();
        DateTime dateTime = (new DateTime(loggerTimestamp)).withZone(DateTimeZone.forID(System.getProperty("json.logger.timezone", "UTC")));
        String timestamp = dateTime.toString();
        if (System.getProperty("json.logger.dateformat") != null && !System.getProperty("json.logger.dateformat").equals("")) {
            timestamp = dateTime.toString(System.getProperty("json.logger.dateformat"));
        }

        Long elapsed = loggerTimestamp - initialTimestamp;
        List<String> disabledFields = config.getJsonOutput().getDisabledFields() != null ? Arrays.asList(config.getJsonOutput().getDisabledFields().split(",")) : new ArrayList();
        log.debug("The following fields will be disabled for logging: " + disabledFields);
        HashMap typedValuesAsString = new HashMap();

        try {
            PropertyUtils.describe(loggerProcessor).forEach((k, v) -> {
                Stream var10000 = disabledFields.stream();
                k.getClass();
                if (var10000.anyMatch(k::equals)) {
                    try {
                        BeanUtils.setProperty(loggerProcessor, k, (Object)null);
                    } catch (Exception var11) {
                        log.error("Failed disabling field: " + k, var11);
                    }
                } else if (v != null) {
                    try {
                        if (v instanceof ParameterResolver) {
                            v = ((ParameterResolver)v).resolve();
                        }

                        if (v.getClass().getCanonicalName().equals("org.mule.runtime.api.metadata.TypedValue")) {
                            log.debug("org.mule.runtime.api.metadata.TypedValue type was found for field: " + k);
                            TypedValue<Object> typedVal = (TypedValue)v;
                            DataType dataType = typedVal.getDataType();
                            Object originalVal = typedVal.getValue();
                            log.debug("Parsing TypedValue for field " + k + " as string for logging...");
                            String stringifiedVal;
                            if (originalVal.getClass().getSimpleName().equals("String")) {
                                stringifiedVal = (String)originalVal;
                            } else {
                                stringifiedVal = (String)this.transformationService.transform(originalVal, dataType, DataType.JSON_STRING);
                            }

                            BeanUtils.setProperty(loggerProcessor, k, (Object)null);
                            typedValuesAsString.put(k, stringifiedVal);
                        }
                    } catch (Exception var10) {
                        log.error("Failed parsing field: " + k, var10);
                        typedValuesAsString.put(k, "Error parsing expression. See logs for details.");
                    }
                }

            });
        } catch (Exception var22) {
            log.error("Unknown error while processing the logger object", var22);
        }

        JsonNode nodeLoggerJson = om.valueToTree(loggerProcessor);
        JsonNode nodeConfigJson = om.valueToTree(config.getGlobalSettings());
        ObjectNode mergedLogger = (ObjectNode)merge(nodeLoggerJson, nodeConfigJson);
        JsonNode typedValuesNode = om.valueToTree(typedValuesAsString);
        mergedLogger.setAll((ObjectNode)typedValuesNode);
        if (elapsed != null) {
            mergedLogger.put("elapsed", Long.toString(elapsed));
        }

        mergedLogger.put("threadName", Thread.currentThread().getName());
        Map<String, String> locationInfo = new HashMap();
        locationInfo.put("location", location.getLocation());
        locationInfo.put("rootContainer", location.getRootContainerName());
        locationInfo.put("component", location.getComponentIdentifier().getIdentifier().toString());
        locationInfo.put("fileName", location.getFileName().orElse(""));
        locationInfo.put("lineInFile", String.valueOf(location.getLineInFile().orElse((Integer) null)));
        if (config.getJsonOutput().isLogLocationInfo()) {
            mergedLogger.putPOJO("locationInfo", locationInfo);
        }

        mergedLogger.put("timestamp", timestamp);
        ObjectWriter ow = config.getJsonOutput().isPrettyPrint() ? om.writer().withDefaultPrettyPrinter() : om.writer();
        String logLine = "";

        try {
            Object obj = om.treeToValue(mergedLogger, Object.class);
            logLine = ow.writeValueAsString(obj);
        } catch (Exception var21) {
            log.error("Error parsing log data as a string", var21);
        }

        this.doLog(loggerProcessor.getPriority().toString(), logLine);
        callback.success(VOID_RESULT);
    }

    private void doLog(String priority, String logLine) {
        byte var4 = -1;
        switch(priority.hashCode()) {
            case 2251950:
                if (priority.equals("INFO")) {
                    var4 = 2;
                }
                break;
            case 2656902:
                if (priority.equals("WARN")) {
                    var4 = 3;
                }
                break;
            case 64921139:
                if (priority.equals("DEBUG")) {
                    var4 = 1;
                }
                break;
            case 66247144:
                if (priority.equals("ERROR")) {
                    var4 = 4;
                }
                break;
            case 80083237:
                if (priority.equals("TRACE")) {
                    var4 = 0;
                }
        }

        switch(var4) {
            case 0:
                jsonLogger.trace(logLine);
                break;
            case 1:
                jsonLogger.debug(logLine);
                break;
            case 2:
                jsonLogger.info(logLine);
                break;
            case 3:
                jsonLogger.warn(logLine);
                break;
            case 4:
                jsonLogger.error(logLine);
        }

    }

    public void initialise() throws InitialisationException {
    }

    private static JsonNode merge(JsonNode mainNode, JsonNode updateNode) {
        Iterator fieldNames = updateNode.fieldNames();

        while(true) {
            while(fieldNames.hasNext()) {
                String updatedFieldName = (String)fieldNames.next();
                JsonNode valueToBeUpdated = mainNode.get(updatedFieldName);
                JsonNode updatedValue = updateNode.get(updatedFieldName);
                if (valueToBeUpdated != null && valueToBeUpdated.isArray() && updatedValue.isArray()) {
                    for(int i = 0; i < updatedValue.size(); ++i) {
                        JsonNode updatedChildNode = updatedValue.get(i);
                        if (valueToBeUpdated.size() <= i) {
                            ((ArrayNode)valueToBeUpdated).add(updatedChildNode);
                        }

                        JsonNode childNodeToBeUpdated = valueToBeUpdated.get(i);
                        merge(childNodeToBeUpdated, updatedChildNode);
                    }
                } else if (valueToBeUpdated != null && valueToBeUpdated.isObject()) {
                    merge(valueToBeUpdated, updatedValue);
                } else if (mainNode instanceof ObjectNode) {
                    ((ObjectNode)mainNode).replace(updatedFieldName, updatedValue);
                }
            }

            return mainNode;
        }
    }

    static {
        om = (new ObjectMapper()).setSerializationInclusion(Include.NON_NULL).configure(MapperFeature.SORT_PROPERTIES_ALPHABETICALLY, true).configure(SerializationFeature.ORDER_MAP_ENTRIES_BY_KEYS, true);
    }
}