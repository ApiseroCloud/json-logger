package org.mule.extension.jsonlogger.internal;

import com.google.common.cache.CacheLoader;
import java.util.concurrent.TimeUnit;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.LoadingCache;
import org.mule.runtime.extension.api.annotation.Operations;
import org.mule.extension.jsonlogger.api.LoggerConfig;

@Operations({ JsonloggerOperations.class })
public class JsonloggerConfiguration extends LoggerConfig
{
    private static LoadingCache<String, Long> timerCache;
    
    public Long getCachedTimerTimestamp(final String key) throws Exception {
        final LoadingCache<String, Long> cache = getTimerCache();
        return (Long)cache.get((String) key);
    }
    
    public static LoadingCache<String, Long> getTimerCache() {
        return JsonloggerConfiguration.timerCache;
    }
    
    public static void invalidateTimerKey(final String key) {
        JsonloggerConfiguration.timerCache.invalidate((Object)key);
    }
    
    public static void printTimerKeys() {
        System.out.println(JsonloggerConfiguration.timerCache.asMap());
    }
    
    static {
        JsonloggerConfiguration.timerCache = (LoadingCache<String, Long>)CacheBuilder.newBuilder().maximumSize(1000000L).expireAfterAccess(3L, TimeUnit.MINUTES).build((CacheLoader)new CacheLoader<String, Long>() {
            public Long load(final String key) throws Exception {
                return System.currentTimeMillis();
            }
        });
    }
}
