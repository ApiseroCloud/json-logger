package org.mule.extension.jsonlogger.internal;

import org.mule.runtime.extension.api.annotation.Configurations;
import org.mule.runtime.extension.api.annotation.Extension;
import org.mule.runtime.extension.api.annotation.dsl.xml.Xml;

@Xml(prefix = "json-logger")
@Extension(name = "JSON Logger")
@Configurations({ JsonloggerConfiguration.class })
public class JsonloggerExtension
{
}
