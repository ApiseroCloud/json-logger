package org.mule.extension.jsonlogger.internal;

public final class JsonloggerConnection
{
    private final String id;
    
    public JsonloggerConnection(final String id) {
        this.id = id;
    }
    
    public String getId() {
        return this.id;
    }
    
    public void invalidate() {
    }
}
